#ifndef EUPDATEABLE_HPP
#define EUPDATEABLE_HPP

#include "Transformation.hpp"

class EUpdateable
{
protected:
	virtual void update(float width, float height ) = 0; 

};

#endif // !EUPDATEABLE_HPP
