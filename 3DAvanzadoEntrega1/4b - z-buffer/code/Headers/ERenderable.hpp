#ifndef ERENDERABLE_HPP
#define ERENDERABLE_HPP

#include "Rasterizer.hpp"
#include "Color_Buffer_Rgba8888.hpp"


class ERenderable
{
protected:
	using Color = example::Color_Buffer_Rgba8888::Color;
	using ColorBuffer = example::Color_Buffer_Rgba8888; 
	using VertexColors = std::vector<Color>;

	virtual void render(example::Rasterizer<ColorBuffer> colors) = 0;

};

#endif // !ERENDERABLE_HPP
