#ifndef ELOGGER_HPP
#define ELOGGER_HPP

#include <iostream>
#include <string>

class ELogger
{
public:
	inline static void logError(std::string error)
	{
		std::cout << "\033[1;31m " + error + " \033[0m" << std::endl;
	}


	inline static void logWarning(std::string error)
	{
		std::cout << "\033[1;33m " + error + " \033[0m" << std::endl;
	}


	inline static void logSuccess(std::string error)
	{
		std::cout << "\033[1;32m" + error + " \033[0m" << std::endl;
	}

	inline static void log(std::string error)
	{
		std::cout << "\033[1;35m" + error + " \033[0m" << std::endl;
	}

};


#endif // !define ELOGGER_HPP



