#ifndef EMESH_HPP
#define EMESH_HPP

#include <vector>


#include <ERenderable.hpp>
#include <EUpdateable.hpp>
#include "Translation.hpp"
#include "Scaling.hpp"


using namespace toolkit; 



class EMesh : public EUpdateable, public ERenderable
{
private:
	using Vertex = toolkit::Point4f;
	using VertexBuffer = std::vector <Vertex>;
	using IndexBuffer = std::vector <int>;



public: 
	EMesh(const std::string &path);

	float &getAngle();

	const std::string &getPath(); 

	bool isFrontFace(const Vertex *  const projected_vertices, const int * const indices);

	void update(float width, float height) override;

	void render(example::Rasterizer<ColorBuffer> colors) override; 

	void setTranslation(Translation3f tr);

	void setScale(Scaling3f scale);


private: 
	const std::string path; 

	float scale;

	///Angle of the mesh
	float angle = 0; 

	///Vertices of the mesh.
	VertexBuffer original_vertices; 

	///Indices of the vertices of the mesh.
	IndexBuffer original_indices; 

	///Colors assigned to each vertex.
	std::vector<Color> original_colors; 

	///Transformed vertices.
	VertexBuffer transformed_vertices; 

	///Transformed colors.
	std::vector <Color> transformed_colors; 

	///display_vertices z_buffer example
	std::vector <Point4i> display_vertices; 

	///Final vertices, those which we want to display
	std::vector <Color> final_colors; 

	///Translation of the mesh.
	Translation3f translation; 

	Scaling3f scaling;
	
};


#endif