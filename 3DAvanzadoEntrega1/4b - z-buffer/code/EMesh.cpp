#include "EMesh.hpp"
#include <iostream>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

#include <ELogger.hpp>
#include "Projection.hpp"
#include "Translation.hpp"
#include "Rotation.hpp"
#include "Scaling.hpp"


using namespace tinyobj;
using namespace std;


EMesh::EMesh(const std::string &path)
	: path(path)
{

	std::string error; 
	int indicesNum;
	// Se definen los datos de los v�rtices:		
	attrib_t             attributes;
	vector< shape_t    > shapes;
	vector< material_t > materials;

	// Se intenta cargar el archivo OBJ:
	if (!tinyobj::LoadObj(&attributes, &shapes, &materials, &error, this->path.c_str()) || !error.empty())
	{
		return;
	}

	if (shapes.size() == 0) { error = string("There're no shapes in ") + this->path; return; }
	if (attributes.vertices.size() == 0) { error = string("There're no vertices in ") + this->path; return; }
	if (attributes.normals.size() == 0) { error = string("There're no normals in ") + this->path; return; }

	indicesNum = 0; //Contador auxiliar de indices por si tiene mas de un shape


	for (auto & shapes : shapes)
	{
		vector< float > vertex_components(shapes.mesh.indices.size() * 3);
		vector< float > normal_components(shapes.mesh.indices.size() * 3);

		const vector< index_t > & indices = shapes.mesh.indices;
		const size_t             indices_count = (size_t)indices.size();

		if (indices_count > 0)
		{
			const size_t   vertices_count = indices_count;

			//Se guardan los vertices, las normales y se ordenan los indices
			for (size_t src = 0, dst = 0; src < indices_count; ++src, dst += 3)
			{
				int vertex_src = indices[src].vertex_index * 3;
				int normal_src = indices[src].normal_index * 3;

				vertex_components[dst + 0] = attributes.vertices[vertex_src + 0];
				vertex_components[dst + 1] = attributes.vertices[vertex_src + 1];
				vertex_components[dst + 2] = attributes.vertices[vertex_src + 2];


				original_indices.push_back(indicesNum + src);
			}
		}

		size_t number_of_vertices = shapes.mesh.indices.size();

		original_vertices.resize(indicesNum + number_of_vertices);

		//Organiza los vertices y las normales
		for (size_t i = 0; i < number_of_vertices; i++)
		{
			original_vertices[indicesNum + i].coordinates()[0] = vertex_components[3 * i + 0];
			original_vertices[indicesNum + i].coordinates()[1] = vertex_components[3 * i + 1];
			original_vertices[indicesNum + i].coordinates()[2] = vertex_components[3 * i + 2];
			original_vertices[indicesNum + i].coordinates()[3] = 1.0f;

		
		}

		//Los vertices, normales y colores deben tener la misma cantidad de elementos
		transformed_vertices.resize(number_of_vertices + indicesNum);
		transformed_colors.resize(number_of_vertices + indicesNum);
		display_vertices.resize(number_of_vertices + indicesNum);

		size_t number_of_colors = number_of_vertices;
		original_colors.resize(number_of_colors + indicesNum);

		//Guarda los colores
		for (size_t index = 0; index < number_of_colors; index++)
		{
			original_colors[index + indicesNum].set(255, 0, 0); 
		}

		indicesNum = indicesNum + indices_count;
	}

}


float & EMesh::getAngle()
{
	return this->angle; 
}


const std::string & EMesh::getPath()
{
	return this->path;
}


bool EMesh::isFrontFace(const Vertex * const projected_vertices, const int * const indices)
{
	const Vertex & v0 = projected_vertices[indices[0]];
	const Vertex & v1 = projected_vertices[indices[1]];
	const Vertex & v2 = projected_vertices[indices[2]];

	// Se asumen coordenadas proyectadas y pol�gonos definidos en sentido horario.
	// Se comprueba a qu� lado de la l�nea que pasa por v0 y v1 queda el punto v2:

	return ((v1[0] - v0[0]) * (v2[1] - v0[1]) - (v2[0] - v0[0]) * (v1[1] - v0[1]) > 0.f);
}


void EMesh::update(float width, float height)
{
	static float angle = 0.f;
	angle += 0.025f;

	// Se crean las matrices de transformaci�n:
	Projection3f projection(5, 15, 20, float(width) / float(height));
	Rotation3f    rotation_x;
	Rotation3f    rotation_y;

	rotation_x.set< Rotation3f::AROUND_THE_X_AXIS >(0.5f);
	rotation_y.set< Rotation3f::AROUND_THE_Y_AXIS >(angle);

	// Creaci�n de la matriz de transformaci�n unificada sin la proyeccion:

	Transformation3f transformation =  translation * rotation_x * rotation_y * scaling;

	// Se transforman todos los v�rtices y las normales usando la matriz de transformaci�n resultante:
	for (size_t index = 0, number_of_vertices = original_vertices.size(); index < number_of_vertices; index++)
	{
		Vertex & vertex = transformed_vertices[index] = Matrix44f(transformation) * Matrix41f(original_vertices[index]);

		vertex = transformed_vertices[index] = Matrix44f(projection) * Matrix41f(transformed_vertices[index]);
		transformed_colors[index].set((uint8_t)((float)original_colors[index].data.component.r), (uint8_t)((float)original_colors[index].data.component.g), (uint8_t)((float)original_colors[index].data.component.b));

		// La matriz de proyecci�n en perspectiva hace que el �ltimo componente del vector
		// transformado no tenga valor 1.0, por lo que hay que normalizarlo dividiendo:
		float divisor = 1.f / vertex[3];
		vertex[0] *= divisor;
		vertex[1] *= divisor;
		vertex[2] *= divisor;
		vertex[3] = 1.f;
	}

}


void EMesh::render(example::Rasterizer<ColorBuffer> rasterizer)
{
	// Se convierten las coordenadas transformadas y proyectadas a coordenadas
	   // de recorte (-1 a +1) en coordenadas de pantalla con el origen centrado.
	   // La coordenada Z se escala a un valor suficientemente grande dentro del
	   // rango de int (que es lo que espera fill_convex_polygon_z_buffer).

	Scaling3f        scaling = Scaling3f(float(rasterizer.get_color_buffer().get_width() / 2), float(rasterizer.get_color_buffer().get_height() / 2), 100000000.f);
	Translation3f    translation = Translation3f(float(rasterizer.get_color_buffer().get_width() / 2), float(rasterizer.get_color_buffer().get_height() / 2), 0.f);
	Transformation3f transformation = translation * scaling;

	for (size_t index = 0, number_of_vertices = transformed_vertices.size(); index < number_of_vertices; index++)
	{
		display_vertices[index] = Point4i(Matrix44f(transformation) * Matrix41f(transformed_vertices[index]));
	}

	//Se dibujan los tri�ngulos:
	for (int * indices = original_indices.data(), *end = indices + original_indices.size(); indices < end; indices += 3)
	{
		if (this->isFrontFace(transformed_vertices.data(), indices))
		{
			// Se establece el color del pol�gono a partir del color de su primer v�rtice:
			rasterizer.set_color(transformed_colors[*indices]);

			// Se rellena el pol�gono:
			rasterizer.fill_convex_polygon_z_buffer(display_vertices.data(), indices, indices + 3);
		}
	}

	// Se copia el frameb�ffer oculto en el frameb�ffer de la ventana:
	rasterizer.get_color_buffer().gl_draw_pixels(0, 0);
	
}

void EMesh::setTranslation(Translation3f tr)
{
	this->translation = tr;
}


void EMesh::setScale(Scaling3f scale)
{
	this->scaling = scale;
}


